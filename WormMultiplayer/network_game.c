//
//  network_game.c
//  WormXCODE
//
//  Created by Rune Frohn on 04.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#include "network_game.h"
#include "worm.h"
#include "menu.h"

void start_network_game(struct networkInfo network_info){
    enum ResCodeNetwork res_code_net = RES_NET_OK;
    enum GameStates game_state;
    enum ResCodes res_code; // Result code from functions
    struct game_options thegops;
    thegops.nap_time = 100;
    thegops.start_single_step = false;
    thegops.start_level_fielname = NULL;
    game_state = WORM_GAME_ONGOING;
    if(network_info.networkside == SIDE_SERVER){
        //exchange basic Information
        //listen for BOUNDS etc.
        doLevel(&thegops, &game_state, NULL, GAME_REMOTE_MULTIPLAYER, &network_info);
    }else if(network_info.networkside == SIDE_CLIENT){
        //exchange basic Information
        //send BOUNDS etc.
        doLevel(&thegops, &game_state, NULL, GAME_REMOTE_MULTIPLAYER, &network_info);
    }
    if(game_state != WORM_GAME_QUIT){
        //ask to play new Round
        char* items[] = {"PLAY AGAIN","QUIT",NULL};
        int res = openSubMenu(LINES/2+2, COLS/2, items,FALSE,NULL);
        if(res == 0){
            menuClearRow(LINES/2+2);
            menuClearRow(LINES/2+4);
            displayStringInCenter((LINES/2), COLS/2, "try to reconnect", COLP_FOOD_2);
            refresh();
            start_network_game(network_info);
        }
    }
}

void net_send_message(struct networkInfo* network_info, enum ResCodeNetwork* res_code, char buffer[256]){
    if(network_info->networkside == SIDE_SERVER){
        struct Server* s = (struct Server*)network_info->networkAdapter;
        enum ServerRes res = RES_SOK;
        sendMessage(&res, s, buffer);
        if(res != RES_SOK){
            *res_code = RES_NET_ERROR;
        }
    }else if(network_info->networkside == SIDE_CLIENT){
        struct Client* s = (struct Client*)network_info->networkAdapter;
        enum ClientRes res = RES_COK;
        writeMessage(&res, s, buffer);
        if(res != RES_COK){
            *res_code = RES_NET_ERROR;
        }
    }
}

void net_listen_message(struct networkInfo* network_info, enum ResCodeNetwork* res_code, char buffer[256]){
    if(network_info->networkside == SIDE_SERVER){
        struct Server* s = (struct Server*)network_info->networkAdapter;
        enum ServerRes res = RES_SOK;
        s_listenForMessage(&res, s, buffer);
        if(res != RES_SOK){
            *res_code = RES_NET_ERROR;
        }
    }else if(network_info->networkside == SIDE_CLIENT){
        struct Client* s = (struct Client*)network_info->networkAdapter;
        enum ClientRes res = RES_COK;
        c_listenForMessage(&res, s, buffer);
        if(res != RES_COK){
            *res_code = RES_NET_ERROR;
        }
    }
}
void net_create_create_worm_message(char* buff,struct pos mypos, enum WormHeading mydir,struct pos otherpos,enum WormHeading otherdir){
    bzero(buff, 256);
    buff[0] = NET_CODE_START_POS_INFORMATION;
    buff[1] = NET_CODE_SPLIT;
    int i ;
    for(i = 0 ; i < 6;i++){
        int val;
        switch (i%3) {
            case 0:
                if(i < 3){
                    val = mypos.y;
                }else{
                    val = otherpos.y;
                }
                break;
            case 1:
                if(i < 3){
                    val = mypos.x;
                }else{
                    val = otherpos.x;
                }
                break;
            case 2:
                if(i < 3){
                    val = mydir;
                }else{
                    val = otherdir;
                }
                break;
                
            default:
                break;
        }
        sprintf(buff, "%s%d%c",buff,val,NET_CODE_SPLIT);
    }
}
void net_createNetWormCode(struct worm* aworm,char buff[256]){
    bzero(buff, 256);
    buff[0] = NET_CODE_WORMPOS;
    buff[1] = NET_CODE_SPLIT;
    sprintf(buff, "%s%d%c%d%c%d%c",buff,aworm->dy,NET_CODE_SPLIT,aworm->dx,NET_CODE_SPLIT,aworm->cur_lastindex,NET_CODE_SPLIT);
}
void net_updateNetWorm(struct worm* aworm,char buff[256]){
    char pat[2];
    int i = 0;
    char* pch;
    pat[0] = NET_CODE_SPLIT;
    pat[1] = '\0';
    //ignor first w:
    buff = buff+2;
    pch = strtok (buff,pat);
    struct food cFood;
    while (pch != NULL)
    {
        switch (i) {
            case 0:
                aworm->dy = atoi(pch);
                break;
            case 1:
                aworm->dx = atoi(pch);
                break;
            case 2:
                aworm->cur_lastindex = atoi(pch);
                break;
            default:
                break;
        }
        pch = strtok (NULL, pat);
        i++;
    }
}
void net_create_board_bounds_massage(char* buff,struct board* theboard){
    bzero(buff, 256);
    buff[0] = NET_CODE_BOARD_BOUNDS;
    buff[1] = NET_CODE_SPLIT;
    sprintf(buff, "%s%d%c%d%c",buff,theboard->last_col,NET_CODE_SPLIT,theboard->last_row,NET_CODE_SPLIT);
}
void net_get_board_bounds(char* buff,int* clientRows,int* clientCols){
    char pat[2];
    int i = 0;
    char* pch;
    pat[0] = NET_CODE_SPLIT;
    pat[1] = '\0';
    //ignor first b:
    buff = buff+2;
    pch = strtok (buff,pat);
    while (pch != NULL)
    {
        switch (i) {
            case 0:
                *clientCols = atoi(pch);
                break;
            case 1:
                *clientRows = atoi(pch);
                break;
        }
        pch = strtok (NULL, pat);
        i++;
    }
}
void net_create_final_board_bounds_massage(char* buff,struct netBoard* theboard){
    bzero(buff, 256);
    buff[0] = NET_CODE_FINAL_BOARD_BOUNDS;
    buff[1] = NET_CODE_SPLIT;
    sprintf(buff, "%s%d%c%d%c",buff,theboard->cols,NET_CODE_SPLIT,theboard->rows,NET_CODE_SPLIT);
}
void net_get_final_board_bounds(char* buff,struct netBoard* theboard){
    char pat[2];
    int i = 0;
    char* pch;
    pat[0] = NET_CODE_SPLIT;
    pat[1] = '\0';
    //ignor first B:
    buff = buff+2;
    pch = strtok (buff,pat);
    while (pch != NULL)
    {
        switch (i) {
            case 0:
                theboard->cols = atoi(pch);
                break;
            case 1:
                theboard->rows = atoi(pch);
                break;
        }
        pch = strtok (NULL, pat);
        i++;
    }
}


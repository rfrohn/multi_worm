//
//  config.h
//  multi_worm
//
//  Created by Rune Frohn on 08.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#ifndef config_h
#define config_h

#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

#define CONF_BUFFER_SIZE 256

struct config_server{
    char* ip;
    int port;
    char* name;
};

struct level{
    int difficultiy;
    char* name;
};

struct config{
    struct config_server* server_list;
    long server_list_size;
    char* user_level_base_dir;
    struct level* level_list;
    long level_list_size;
    char* filename;
    
};
enum config_res{
    RES_CONF_OK,
    RES_CONF_FAILED_TO_OPEN_FILE,
    RES_BAD_STRUCTURE,
    RES_BAD_LIST_SIZE,
    RES_FAILED_TO_MALLOC,
};
enum list_state{
    LIST_STATE_NONE,
    LIST_STATE_LEVEL,
    LIST_STATE_SERVER,
};
extern bool startsWith(const char *str,const char *pre);
extern enum config_res writeConfig(struct config* conf);
extern enum config_res getConfig(struct config* conf,char* filename);
extern void cleanUpConfig(struct config* conf);
extern void config_add_Server(struct config* conf, char* ip, int port, char name[]);
extern void config_remove_Server(struct config* conf,int i);
#endif /* config_h */

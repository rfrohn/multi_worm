//
//  config.c
//  multi_worm
//
//  Used to read and write the user config file
//
//  Created by Rune Frohn on 08.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#include "config.h"
#include "messages.h"
#include <string.h>

enum config_res getConfig(struct config* conf,char* filename){// Open the file
    //char buf[100];  // for messages
    char buffer[CONF_BUFFER_SIZE];
    FILE* in;       // FILE pointer for reading from file
    enum list_state cListState = LIST_STATE_NONE;
    bool allocLevel = false;
    bool allocServer = false;
    conf->filename = filename;
    if ( (in = fopen(filename,"r")) == NULL) {
        /*sprintf(buf,"Kann Datei %s nicht oeffnen",filename);
        showDialog(buf,"Bitte eine Taste druecken");*/
        conf->server_list_size = 0;
        conf->server_list = NULL;
        conf->level_list_size = 0;
        conf->level_list = NULL;
        conf->user_level_base_dir = malloc(sizeof(char)*strlen("./")+1);
        conf->user_level_base_dir = "./";
        return writeConfig(conf);
        return RES_CONF_FAILED_TO_OPEN_FILE;
    }
    // Read one line from file
    while (fgets(buffer,CONF_BUFFER_SIZE,in) != NULL) {
        // The specifiction of fgets guarantees that there are
        // at least two characters in the buffer, namely '\n' followed by '\0'.
        // Exception: buffer was full before '\n' was read. Then we only
        // have a '\0' in the buffer.
        int currentListIndex;
        if(cListState != LIST_STATE_NONE){
            if(buffer[0] == '+'){
                buffer[strlen(buffer)-1] = '\0';
                switch(cListState){
                    case LIST_STATE_LEVEL:
                        //add Element to Level List
                        if(allocLevel){
                            char pat[2];
                            int i = 0;
                            char* pch;
                            pat[0] = ' ';
                            pat[1] = '\0';
                            //ignor first +
                            pch = strtok (buffer+1,pat);
                            while (pch != NULL)
                            {
                                switch (i) {
                                    case 0:
                                        conf->level_list[currentListIndex].name = malloc(sizeof(char)*strlen(pch)+1);
                                        if(conf->level_list[currentListIndex].name == NULL){
                                            return RES_FAILED_TO_MALLOC;
                                        }else{
                                            strcpy(conf->level_list[currentListIndex].name, pch);
                                        }
                                        break;
                                    case 1:
                                        conf->level_list[currentListIndex].difficultiy = atoi(pch);
                                        break;
                                }
                                pch = strtok (NULL, pat);
                                i++;
                            }
                            currentListIndex++;
                        }else{
                            return RES_BAD_STRUCTURE;
                        }
                        break;
                    case LIST_STATE_SERVER:
                        //add Element to Server List
                        if(allocServer){
                            char pat[3];
                            int i = 0;
                            char* pch;
                            pat[0] = ':';
                            pat[1] = ' ';
                            pat[2] = '\0';
                            //ignor first +
                            pch = strtok (buffer+1,pat);
                            while (pch != NULL)
                            {
                                switch (i) {
                                    case 0:
                                        conf->server_list[currentListIndex].ip = malloc(sizeof(char)*(strlen(pch)+1));
                                        if(conf->server_list[currentListIndex].ip == NULL){
                                            return RES_FAILED_TO_MALLOC;
                                        }else{
                                            strcpy(conf->server_list[currentListIndex].ip, pch);
                                        }
                                        break;
                                    case 1:
                                        conf->server_list[currentListIndex].port = atoi(pch);
                                        break;
                                    case 2:
                                        conf->server_list[currentListIndex].name = malloc(sizeof(char)*(strlen(pch)+1));
                                        if(conf->server_list[currentListIndex].name == NULL){
                                            return RES_FAILED_TO_MALLOC;
                                        }else{
                                            strcpy(conf->server_list[currentListIndex].name, pch);
                                        }
                                        break;
                                }
                                pch = strtok (NULL, pat);
                                i++;
                            }
                            currentListIndex++;
                        }else{
                            return RES_BAD_STRUCTURE;
                        }
                        break;
                    default:
                        //NOT GOOD :D
                        break;
                    
                }
            }else{
                cListState = LIST_STATE_NONE;
            }
        }
        //get current identifier:
        if(startsWith(buffer, "server:")){
            char* ns = buffer + strlen("server:");
            ns[strlen(ns)-1] = '\0';
            char* ret = malloc(sizeof(char)*strlen(ns));
            char** rett = &ret;
            long size = strtoimax(ns,rett,10);
            conf->server_list_size = size;
            if(ns[0] != '\0' && **rett == '\0'){
            //valid!
                conf->server_list = malloc(sizeof(struct config_server)*size);
                if(conf->server_list == NULL){
                    return RES_FAILED_TO_MALLOC;
                }else{
                    bzero(conf->server_list, sizeof(struct config_server)*size);
                    currentListIndex = 0;
                    allocServer = true;
                    cListState = LIST_STATE_SERVER;
                }

            }else{
                return RES_BAD_LIST_SIZE;
            }
        }else if(startsWith(buffer, "level:")){
            char* ns = buffer + strlen("level:");
            ns[strlen(ns)-1] = '\0';
            char* ret = malloc(sizeof(char)*strlen(ns));
            char** rett = &ret;
            long size = strtoimax(ns,rett,10);
            conf->level_list_size = size;
            if(ns[0] != '\0' && **rett == '\0'){
                //valid!
                conf->level_list = malloc(sizeof(struct level)*size);
                if(conf->level_list == NULL){
                    return RES_FAILED_TO_MALLOC;
                }else{
                    bzero(conf->level_list, sizeof(struct level)*size);
                    currentListIndex = 0;
                    allocLevel = true;
                    cListState = LIST_STATE_LEVEL;
                }
                
            }else{
                return RES_BAD_LIST_SIZE;
            }
        }else if(startsWith(buffer, "level_base_dir:")){
            char* ns = buffer + strlen("level_base_dir:");
            ns[strlen(ns)-1] = '\0';
            long size = strlen(ns)+1;
            conf->user_level_base_dir = malloc(sizeof(char)*size);
            if(conf->user_level_base_dir == NULL){
                return RES_FAILED_TO_MALLOC;
            }
            strcpy(conf->user_level_base_dir,ns);
        }
    }
    return RES_CONF_OK;
}

enum config_res writeConfig(struct config* conf){
    FILE *f = fopen(conf->filename, "w");
    if (f == NULL)
    {
        return RES_CONF_FAILED_TO_OPEN_FILE;
    }
    
    /* print filname:... */
    fprintf(f, "filename:%s\n", conf->filename);
    
    /* print level_base_dir: */
    fprintf(f, "level_base_dir:%s\n", conf->user_level_base_dir);
    
    /* printing levels */
    fprintf(f, "level:%ld\n", conf->level_list_size);
    int i;
    for(i=0;i<conf->level_list_size;i++){
        fprintf(f, "+%s %d\n", conf->level_list[i].name,conf->level_list[i].difficultiy);
    }
    // print servers
    fprintf(f, "server:%ld\n", conf->server_list_size);
    for(i=0;i<conf->server_list_size;i++){
        fprintf(f, "+%s:%d %s\n", conf->server_list[i].ip,conf->server_list[i].port,conf->server_list[i].name);
    }
    fclose(f);
    return RES_CONF_OK;
}

bool startsWith(const char *str,const char *pre)
{
    size_t lenpre = strlen(pre),
           lenstr = strlen(str);
    return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
}
void config_add_Server(struct config* conf, char* ip, int port, char name[]){
    //check if new Server is already in List
    int i;
    bool exist = false;
    for(i = 0; i < conf->server_list_size;i++){
        if(strcmp(conf->server_list[i].ip, ip) && conf->server_list[i].port == port){
            exist = true;
            break;
        }
    }
    // add the Server to the List
    if(!exist){
        //copy array in a new One with size+1;
        long newSize = conf->server_list_size+1;
        //allocate new Array Space;
        struct config_server* newServerList = malloc(sizeof(struct config_server)*newSize);
        if(newServerList != NULL){
            //copy new Values into new ServerList
            for(i = 0; i < conf->server_list_size;i++){
                newServerList[i] = conf->server_list[i];
            }
            free(conf->server_list);
            //add new Element
            newServerList[i].ip = malloc(sizeof(char)*(strlen(ip)+1));
            if(newServerList[i].ip != NULL){
                strcpy(newServerList[i].ip, ip);
                newServerList[i].port = port;
                newServerList[i].name = malloc(sizeof(char)*(strlen(name)+1));
                if(newServerList[i].name != NULL){
                    strcpy(newServerList[i].name, name);
                    conf->server_list = newServerList;
                    conf->server_list_size = newSize;
                    //Save the new List:
                    writeConfig(conf);
                }else{
                    //error
                    exit(1);
                }
            }else{
                //error
                exit(1);
            }
        }else{
            //error
            exit(1);
        }
    }
    
}
void config_remove_Server(struct config* conf,int i){
    if(i<conf->server_list_size){
        int newSize = (conf->server_list_size)-1;
        struct config_server* newServerList = malloc(sizeof(struct config_server)*newSize);
        if(newServerList != NULL){
            int newListIndex = 0;
            int oldListIndex;
            for(oldListIndex = 0;oldListIndex<conf->server_list_size;oldListIndex++){
                if(oldListIndex != i){
                    newServerList[newListIndex] =  conf->server_list[oldListIndex];
                    newListIndex++;
                }
            }
            free(conf->server_list);
            conf->server_list = newServerList;
            conf->server_list_size = newSize;
            writeConfig(conf);
        }
    }
    
}
void cleanUpConfig(struct config* conf){
    int i;
    for(i=0;i<conf->level_list_size;i++){
        free(conf->level_list[i].name);
    }
    free(conf->level_list);
    for(i=0;i<conf->server_list_size;i++){
        free(conf->server_list[i].name);
        free(conf->server_list[i].ip);
    }
    free(conf->server_list);
    free(conf->user_level_base_dir);
    
}
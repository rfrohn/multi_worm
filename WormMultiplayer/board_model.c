// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The board model


#include <curses.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "worm.h"
#include "board_model.h"
#include "messages.h"
//#include "network_game.h"

// *************************************************
// Placing and removing items from the game board
// Check boundaries of game board
// *************************************************

// Place an item onto the curses display.
void placeItem(struct board* aboard, int y, int x, enum BoardCodes board_code,chtype symbol,enum ColorPairs color_pair) {
  //  Store item on the display (symbol code)
  move(y, x);                         // Move cursor to (y,x)
  attron(COLOR_PAIR(color_pair));     // Start writing in selected color
  addch(symbol);                      // Store symbol on the virtual display
  attroff(COLOR_PAIR(color_pair));    // Stop writing in selected color
  aboard->cells[y][x]=board_code;
}

// Read level decription from file
// We allow for level descriptions of dimensions
//    (aboard->last_row + 1) x (last_col + 1)
enum ResCodes initializeLevelFromFile(struct board* aboard, const char* filename) {
    //napms(5000);
    int y,x;
    int rownr;
    char buf[100];  // for messages
    FILE* in;       // FILE pointer for reading from file
    
    // Fill board with empty cells.
    for (y = 0; y <= aboard->last_row; y++) {
        for (x = 0; x <= aboard->last_col; x++) {
            placeItem(aboard,y,x,BC_FREE_CELL,SYMBOL_FREE_CELL,COLP_FREE_CELL);
        }
    }
    
    // Initialize food_items
    aboard->food_items = 0;
    
    // Read at most aboard->last_row+1 lines from file
    // Read at most aboard->last_col+1 characters per line from file
    // --> We need a buffer of size (aboard->last_col+1 + 2 )
    // The additional 2 elements are for '\n' and '\0'
    int bufsize = aboard->last_col + 3;
    char* buffer;
    if ((buffer = malloc(sizeof(char) * bufsize)) == NULL) {
        sprintf(buf,"Kein Speicher mehr in initializeLevelFromFile\n");
        showDialog(buf,"Bitte eine Taste druecken");
        return RES_FAILED;
    }
    
    // Open the file
    if ( (in = fopen(filename,"r")) == NULL) {
        sprintf(buf,"Kann Datei %s nicht oeffnen",filename);
        showDialog(buf,"Bitte eine Taste druecken");
        return RES_FAILED;
    }
    
    rownr = 0;
    // Read all lines from the text file describing the level
    while (rownr < aboard->last_row+1 && ! feof(in)) {
        int len;
        // Read one line from file
        if (fgets(buffer,bufsize,in) != NULL) {
            // The specifiction of fgets guarantees that there are
            // at least two characters in the buffer, namely '\n' followed by '\0'.
            // Exception: buffer was full before '\n' was read. Then we only
            // have a '\0' in the buffer.
            len = strlen(buffer);
            // Note: function strlen does not count the terminating '\0'.
            if (buffer[len -1] != '\n') {
                // Input line was not yet finished. No '\n' in buffer.
                // Delete the last char before '\0'from the buffer because
                // it exceeds the allowed number of characters.
                buffer[len -1] = '\0';
                // Delete the rest of the line that is already in OS input buffer
                while (fgetc(in) != '\n'){;}
            } else {
                // Input line in buffer ends with '\n'
                // Delete the '\n' from the buffer
                buffer[len -1] = '\0';
            }
        } else {
            // fgets returned NULL
            // The reason may be either End Of File (EOF) or a real read error.
            // We immediately return on real read error
            if (!feof(in)) {
                char buf[100];
                sprintf(buf,"Fehler beim Lesen von Zeile %d aus Datei %s",
                        rownr +1,filename);
                showDialog(buf,"Bitte eine Taste druecken");
                return RES_FAILED;
            } else {
                // We got EOF, skip rest of loop
                // Loop condition will fire
                continue;
            }
        }
        // If we reach this line, our buffer is filled with the current input line.
        // Due to our logic above the variable len is properly set.
        // However, for the sake of clarity we compute the length again
        len = strlen(buffer);
        
        // Fill the board's row with the symbols specified in the current input line
        for (x = 0; x <= aboard->last_col && x < len; x++) {
            switch (buffer[x]) {
                case SYMBOL_BARRIER:
                    placeItem(aboard,rownr,x,BC_BARRIER,SYMBOL_BARRIER,COLP_BARRIER);
                    break;
                case SYMBOL_FOOD_1:
                    placeItem(aboard,rownr,x,BC_FOOD_1,SYMBOL_FOOD_1,
                              COLP_FOOD_1);
                    aboard->food_items++;
                    break;
                case SYMBOL_FOOD_2:
                    placeItem(aboard,rownr,x,BC_FOOD_2,SYMBOL_FOOD_2,COLP_FOOD_2);
                    aboard->food_items++;
                    break;
                case SYMBOL_FOOD_3:
                    placeItem(aboard,rownr,x,BC_FOOD_3,SYMBOL_FOOD_3,COLP_FOOD_3);
                    aboard->food_items++;
                    break;
                    
                    // We ignore all other symbols!
            }
        }
        // advance to next input line
        rownr++;
    } // END while
    
    // Free the line buffer
    free(buffer);
    
    // Draw a line in order to separate the message area
    // Note:
    // we cannot use function placeItem() since the message area is outside the board!
    for (x=0; x <= aboard->last_col; x++) {
        move (y, x) ;
        attron(COLOR_PAIR(COLP_BARRIER));
        addch(SYMBOL_BARRIER);
        attroff(COLOR_PAIR(COLP_BARRIER));
    }
    
    fclose(in);
    return RES_OK;
}
// Getters
enum ResCodes initializeLevelForMuliplayer(struct netBoard* aboard){
    //getch();
    //napms(1000);
    int x,y;
    //calc size of the playground
    int starty,startx,endy,endx;
    int offx = (aboard->board->last_col-aboard->cols)/2;
    startx = offx;
    endx = startx+aboard->cols;
    int offy = (aboard->board->last_row-aboard->rows)/2;
    starty = offy;
    endy = starty+aboard->rows;
    
    // define local variables for loops etc
    // Fill board and screen buffer with empty cells.
    for (y = 0; y<=aboard->board->last_row; y++) {
        for (x = 0; x <=aboard->board->last_col ; x++) {
            if(((x == startx || x == endx)&& y>=starty && y<=endy) || ((y == starty || y == endy) && x >= startx && x <= endx)){
                placeItem(aboard->board, y, x, BC_BARRIER, SYMBOL_BARRIER, COLP_BARRIER);
            }else{
                placeItem(aboard->board,y,x,BC_FREE_CELL,SYMBOL_FREE_CELL,COLP_FREE_CELL);
            }
        }
    }
    // Draw a line in order to separate the message area
    // Note: we cannot use function placeltem() since the message area
    // is outside the board!
    y = aboard->board->last_row + 1;
    for (x=0; x <= aboard->board->last_col; x++) {
        move (y, x) ;
        attron(COLOR_PAIR(COLP_BARRIER));
        addch(SYMBOL_BARRIER);
        attroff(COLOR_PAIR(COLP_BARRIER));
    }
    // Draw a line to signal the rightmost column of the board.
    // Barriers: use a loop
    /*for (y=10; y <20;y++ ){
        x = 5;
        placeItem(aboard->board,y,x,BC_BARRIER,SYMBOL_BARRIER,COLP_BARRIER);
    }
    for (x=10; x< 23;x++ ) {
        y = 7;
        placeItem(aboard->board,y,x,BC_BARRIER,SYMBOL_BARRIER,COLP_BARRIER);
    }*/
    // Food
    return RES_OK;
}

/*
 Create Some food on the board.
    @parms
    buff: returns Network ready string (decode with addFood(...)) String Format:[<NET_CODE_ADD_FOOD>:<amoutFood>:y1:y2:value1:y2(...):]
    wantedFoodItems: the amout of food Items to create
    sizeY: Y size of the board
    sizeX: X size of the board
 */

void createFood(char * buff, int wantedFoodItems,int sizeY, int sizeX){
    struct food* foodElements;
    foodElements = malloc(sizeof(struct food)*wantedFoodItems);
    int cFoodItems = 0;
    while(cFoodItems < wantedFoodItems){
        //Calc position for Food Item
        struct pos next_pos;
        int seed = time(NULL);
        srand(seed);
        next_pos.x = rand() % sizeX;
        next_pos.y = rand() % sizeY;
        //Check if position is not in Use
        bool coll = false;
        int i;
        for(i=0;i < cFoodItems;i++){
            if(next_pos.x == foodElements[i].pos.x && next_pos.y == foodElements[i].pos.y){
                coll = true;
                break;
            }
        }
        if(!coll){
            //if Free
            //Calc food category between 0,2
            int foodLevel = rand() %3;
            //Get BoardCodes, symbol and color pair
          
            //place Item
            struct food new_food;
            new_food.pos = next_pos;
            new_food.level = foodLevel;
            foodElements[cFoodItems] = new_food;
            //increment cFoodItems
            cFoodItems++;
        }
    }
    //create Massage String
    bzero(buff, 256);
    sprintf(buff, "%c%c%d%c",NET_CODE_ADD_FOOD,NET_CODE_SPLIT,cFoodItems,NET_CODE_SPLIT);
    int i;
    for(i = 0; i < cFoodItems; i++){
       sprintf(buff, "%s%d%c%d%c%d%c",buff,foodElements[i].pos.y,NET_CODE_SPLIT,foodElements[i].pos.x,NET_CODE_SPLIT,foodElements[i].level,NET_CODE_SPLIT);
    }
    free(foodElements);
}
/*
    Add the Food requested by the server to the board
    @parms
    aboard: the board to place the items on;
    buff: Network string with food information (likely encoded by create food)
 */
void addFood(struct board* aboard,char buff[256]){
    int i = -1;
    int foods = 0;
    char pat[2];
    char* pch;
    pat[0] = NET_CODE_SPLIT;
    pat[1] = '\0';
    pch = strtok (buff,pat);
    struct food cFood;
    while (pch != NULL)
    {
        if(i == -1){
            foods = atoi(pch);
        }else{
            switch (i%3) {
                case 0:
                    cFood.pos.y = atoi(pch)+aboard->offset_row;
                    break;
                case 1:
                    cFood.pos.x = atoi(pch)+aboard->offset_col;
                    break;
                case 2:
                    cFood.level = atoi(pch);
                    enum BoardCodes board_code;
                    chtype symbol;
                    enum ColorPairs color_pair;
                    switch(cFood.level){
                        case 2:
                            board_code = BC_FOOD_3;
                            symbol = SYMBOL_FOOD_3;
                            color_pair = COLP_FOOD_3;
                            break;
                        case 1:
                            board_code = BC_FOOD_2;
                            symbol = SYMBOL_FOOD_2;
                            color_pair = COLP_FOOD_2;
                            break;
                        case 0:
                        default:
                            board_code = BC_FOOD_1;
                            symbol = SYMBOL_FOOD_1;
                            color_pair = COLP_FOOD_1;
                    }
                    placeItem(aboard, cFood.pos.y, cFood.pos.x, board_code, symbol, color_pair);
                    break;
                default:
                    break;
            }
        }
        pch = strtok (NULL, pat);
        i++;
    }
}

// Get the last usable row on the display
int getLastRowOnBoard(struct board* aboard) {
  return aboard->last_row;
}

// Get the last usable column on the display
int getLastColOnBoard(struct board* aboard) {
  return aboard->last_col;
}
int getNumberOfFoodItems(struct board* aboard){
  return aboard->food_items;
}
enum ResCodes initializeBoard(struct board* aboard) {
    //napms(1000);
    int y;
    int sizeRow = LINES - ROWS_RESERVED;
    int sizeCol = COLS;
    aboard->last_row = sizeRow - 1;
    aboard->last_col = sizeCol - 1;
  // Check dimensions of the board
  if ( COLS < MIN_NUMBER_OF_COLS ||
      LINES < MIN_NUMBER_OF_ROWS) {
    char buf [100];
    sprintf(buf,"Das Fenster ist zu klein: wir brauchen %dx%d",
        MIN_NUMBER_OF_COLS,MIN_NUMBER_OF_ROWS + ROWS_RESERVED) ;
    showDialog(buf,"Bitte eine Taste druecken");
    return RES_FAILED;
  }
    aboard->cells = malloc(sizeof(enum BoardCodes*) * sizeRow);
    if(aboard->cells == NULL){
        showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste Drücken");
        exit(RES_FAILED); //No memory -> direct exit
    }
    for(y = 0; y < sizeRow; y++){
        aboard->cells[y] = malloc(sizeof(enum BoardCodes*) * sizeCol);
        if(aboard->cells[y] == NULL){
            showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste Drücken");
            exit(RES_FAILED); //No memory -> direct exit
        }
    }
  return RES_OK;
}

void cleanupBoard(struct board* aboard){
    int row;
    for(row = 0; row <= aboard->last_row;row++){
        free(aboard->cells[row]);
    }
    free(aboard->cells);
}

void setNumberOfFoodItems(struct board* aboard, int n){
  aboard->food_items = n;
}
void decrementNumberOfFoodItems(struct board* aboard){
  aboard->food_items--;
}
enum BoardCodes getContentAt(struct board* aboard, struct pos position){
  return aboard->cells[position.y][position.x];
}


//
//  client.h
//  WormXCODE
//
//  Created by Rune Frohn on 04.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#ifndef client_h
#define client_h

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

struct Client {
    int sockfd;
};
enum ClientRes{
    RES_ERROR_C_OPEN_SOCKET,
    RES_ERROR_NO_HOST,
    RES_ERROR_C_CONNECT,
    RES_ERROR_READING,
    RES_ERROR_SEND,
    RES_COK
};

extern void open_client(enum ClientRes* res_code, struct Client* aClient,char* hostname, int port);
extern void c_listenForMessage(enum ClientRes* res_code,struct Client* aClient,char buffer[256]);
extern void writeMessage(enum ClientRes* res_code,struct Client*aClient,char* s);
void closeConnection(struct Client* aClient);
#endif /* client_h */

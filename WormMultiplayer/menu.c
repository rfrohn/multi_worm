//
//  menu.c
//  WormXCODE
//
//  Created by Rune Frohn on 03.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#include "menu.h"
#include "worm.h"
#include "server.h"
#include "client.h"
#include "config.h"



void menuplaceItem(int y, int x, chtype symbol,enum ColorPairs color_pair) {
    //  Store item on the display (symbol code)
    move(y, x);                         // Move cursor to (y,x)
    attron(COLOR_PAIR(color_pair));     // Start writing in selected color
    addch(symbol);                      // Store symbol on the virtual display
    attroff(COLOR_PAIR(color_pair));    // Stop writing in selected color
}

void menuClearRow(int row){
    int i;
    for(i =0; i < COLS; i++){
        menuplaceItem(row, i, ' ', COLP_CURSER_OFF);
    }
}

void displayStringInCenter(int y, int x, char* s, enum ColorPairs color_pair){
    int len = strlen(s);
    int startx = x -(len/2);
    int endx = startx + len;
    int i = 0;
    for(x = startx; x < endx; x++){
        menuplaceItem(y, x, s[i], color_pair);
        i++;
    }
}

void start_Menu(struct config* conf){
    enum ColorPairs cColorPair = COLP_USER_WORM;
    enum WormMove moves[] = {
        MOVES
    };
    int i,x,y;
    
    y = LINES/2;
    x = 0;
    i = 0;
    //napms(5000);
    while (x < (COLS/2)-25) {
        menuplaceItem(y, x, 'o', cColorPair);
        refresh();
        napms(10);
        x++;
    }
    while(moves[i] != FINISH){
        menuplaceItem(y, x, 'o', cColorPair);
        refresh();
        switch (moves[i]) {
            case MOVE_UP:
                y--;
                break;
            case MOVE_DOWN:
                y++;
                break;
            case MOVE_RIGHT:
                x++;
                break;
            case MOVE_LEFT:
                x--;
                break;
            case CHANGE_COLOR_GREEN:
                cColorPair = COLP_USER_WORM;
                break;
            case CHANGE_COLOR_GREY:
                cColorPair = COLP_USER_WORM_C1;
            default:
                break;
        }
        i++;
        napms(10);
    }
    while (x < COLS) {
        menuplaceItem(y, x, 'o', cColorPair);
        refresh();
        napms(5);
        x++;
    }
    //display Singleplayer
    char* menuItems[] = {"SINGLEPLAYER","MULTIPLAYER","QUIT",0};
    char ip[50];
    //ip[0] = '\0';
    bzero(ip, 50);
    char port[10];
    //port[0] = '\0';
    bzero(port, 10);
    char name[50];
    //name[0] = '\0';
    bzero(name, 50);
    do{
        displayStringInCenter(LINES-1, COLS/2, "by RUNE FROHN", COLP_RUNE);
        i = openSubMenu((LINES/2)+2,COLS/2,menuItems,FALSE,NULL);
        int j;
        for(j=0;j<7;j++){
            menuClearRow((LINES/2)+1+j);
        }
        //SINGLEPLAYER
        if(i == 0){
            //show List of all Levels
            char** items = malloc(sizeof(char*)*(conf->level_list_size+2));
            long items_size =conf->level_list_size;
            items[items_size+1]= NULL;
            if(items != NULL){
                int cLevelIndex;
                for(cLevelIndex = 0; cLevelIndex < conf->level_list_size;cLevelIndex++){
                    char name[256];
                    bzero(name, 256);
                    sprintf(name, "%s (%d)",conf->level_list[cLevelIndex].name,conf->level_list[cLevelIndex].difficultiy);
                    items[cLevelIndex] = malloc(sizeof(char)*(strlen(name)+1));
                    strcpy(items[cLevelIndex], name);
                }
                items[conf->level_list_size] = "add new Level File";
                int selctedLevel = openSubMenu((LINES/2)+2, COLS/2, items,FALSE,NULL);
                if(selctedLevel < items_size){
                    //open File
                    struct game_options thegops;
                    thegops.nap_time = 100;
                    thegops.start_single_step = false;
                    thegops.start_level_fielname = NULL;
                    enum GameStates game_state = WORM_GAME_ONGOING;
                    char* filename = malloc(sizeof(char)*(strlen(conf->user_level_base_dir)+strlen(conf->level_list[selctedLevel].name)+1));
                    sprintf(filename, "%s%s",conf->user_level_base_dir,conf->level_list[selctedLevel].name);
                    nodelay(stdscr, TRUE);
                    doLevel(&thegops, &game_state, filename, GAME_LOCAL, NULL);
                    free(filename);
                    nodelay(stdscr, FALSE);
                }else{
                    //add another File
                    char file[50];
                    bzero(file, 50);
                    open_Dialog((LINES/2)+2, COLS/2, file, "file name:");
                    //config_add_Level(conf,file);
                }
                for(cLevelIndex = 0; cLevelIndex < items_size;cLevelIndex++){
                    free(items[cLevelIndex]);
                }
                free(items);
            }
            
            //show List of all Levels
        }else if(i == 1){
            //MULTIPLAYER
            do{
                char* menuItems2[] = {"HOST GAME","JOIN GAME","LOCAL GAME","BACK",NULL};
                i = openSubMenu((LINES/2)+2,COLS/2,menuItems2,FALSE,NULL);
                for(j=0;j<7;j++){
                    menuClearRow((LINES/2)+2+j);
                }
                menuClearRow((LINES/2)+6);
                if(i == 0){
                    //HOST GAME
                    char port[10];
                    bzero(port, 10);
                    port[0]= '\0';
                    open_Dialog((LINES/2)+2, COLS/2, port, "enter port(press Enter to confirm):");
                    menuClearRow((LINES/2)+2);
                    enum ServerRes res_code = RES_SOK;
                    struct Server theServer;
                    open_server(&res_code, &theServer, strtoumax(port,NULL,10));
                    if(res_code == RES_SOK){
                        displayStringInCenter((LINES/2)+1, COLS/2, "waiting for players to connect", COLP_USER_WORM);
                        refresh();
                        listenForClients(&res_code, &theServer);
                        if(res_code == RES_SOK){
                            menuClearRow((LINES/2)+1);
                            displayStringInCenter((LINES/2)+1, COLS/2, "connected to Client", COLP_USER_WORM);
                            refresh();
                            char buff2[256];
                            s_listenForMessage(&res_code, &theServer, buff2);
                            menuClearRow((LINES/2)+1);
                            if(res_code == RES_SOK){
                                char buff[256];
                                sprintf(buff, "got Massage: \"%s\"",buff2);
                                displayStringInCenter((LINES/2)+1, COLS/2, buff, COLP_USER_WORM);
                                struct networkInfo net_info;
                                net_info.networkside = SIDE_SERVER;
                                net_info.networkAdapter = &theServer;
                                start_network_game(net_info);
                                refresh();
                            }else{
                                displayStringInCenter((LINES/2)+1, COLS/2, "Faild to read Massage", COLP_USER_WORM);
                            }
                        }else{
                            displayStringInCenter((LINES/2)+1, COLS/2, "failed to connect to Client", COLP_USER_WORM);
                        }
                    }else{
                        displayStringInCenter((LINES/2)+1, COLS/2, "failed to open port", COLP_USER_WORM);
                    }
                    refresh();
                    
                    //displayStringInCenter((LINES/2)+1, COLS/2, "connection Found", COLP_USER_WORM);
                    refresh();
                    napms(2000);
                    closeServer(&theServer);
                    
                    
                }else if(i == 1){
                    // JOIN GAME
                    //get the server list
                    char** items = malloc(sizeof(char*)*(conf->server_list_size+2));
                    int items_size =conf->server_list_size;
                    items[items_size+1]= NULL;
                    if(items != NULL){
                        int cServerIndex;
                        for(cServerIndex = 0; cServerIndex < conf->server_list_size;cServerIndex++){
                            char name[256];
                            bzero(name, 256);
                            sprintf(name, "%s (%s,%d)",conf->server_list[cServerIndex].name,conf->server_list[cServerIndex].ip,conf->server_list[cServerIndex].port);
                            items[cServerIndex] = malloc(sizeof(char)*(strlen(name)));
                            strcpy(items[cServerIndex], name);
                        }
                        items[conf->server_list_size] = "connect to other";
                        int selctedServer = openSubMenu((LINES/2)+2, COLS/2, items,TRUE,conf);
                        // if user selected Something (is >= 0 when User deleted one Entry => we need to redraw)
                        if(selctedServer >= 0 ){
                            // if other Server...
                            if(selctedServer == conf->server_list_size){
                                open_Dialog((LINES/2)+2, COLS/2, ip, "enter ip(press Enter to confirm):");
                                open_Dialog((LINES/2)+2, COLS/2, port, "enter port(press Enter to confirm):");
                                open_Dialog((LINES/2)+2, COLS/2, name, "wanna name dat Serva :D :");
                                config_add_Server(conf, ip, atoi(port), name);
                                displayStringInCenter((LINES/2)+1, COLS/2, "try to connect...", COLP_USER_WORM);
                                refresh();
                            }else{
                                //else load from conf
                                strcpy(ip,conf->server_list[selctedServer].ip);
                                sprintf(port,"%d",conf->server_list[selctedServer].port);
                                char temp[50];
                                bzero(temp, 50);
                                sprintf(temp,"try to Connect to:%s",items[selctedServer]);
                                displayStringInCenter((LINES/2)+1, COLS/2, temp, COLP_USER_WORM);
                                refresh();
                            }
                            enum ClientRes res_code = RES_COK;
                            struct Client theClient;
                            open_client(&res_code, &theClient, ip, strtoumax(port,NULL,10));
                            if(res_code == RES_COK){
                                displayStringInCenter((LINES/2)+1, COLS/2, "Server Found", COLP_USER_WORM);
                                refresh();
                                napms(50);
                                writeMessage(&res_code, &theClient, "Hallo Test");
                                menuClearRow((LINES/2)+1);
                                if(res_code == RES_COK){
                                    displayStringInCenter((LINES/2)+1, COLS/2, "connected...", COLP_USER_WORM);
                                    refresh();
                                    struct networkInfo net_info;
                                    net_info.networkside = SIDE_CLIENT;
                                    net_info.networkAdapter = &theClient;
                                    start_network_game(net_info);
                                }else{
                                    displayStringInCenter((LINES/2)+1, COLS/2, "failed sendMassage", COLP_USER_WORM);
                                }
                            }else{
                                displayStringInCenter((LINES/2)+1, COLS/2, "failed creat Connection", COLP_USER_WORM);
                            }
                            refresh();
                            napms(2000);
                            closeConnection(&theClient);
                            for(cServerIndex = 0; cServerIndex < items_size;cServerIndex++){
                                free(items[cServerIndex]);
                            }
                            free(items);
                        }
                    }
                    
                }else if(i == 2){
                    //start Local Game
                    struct game_options thegops;
                    thegops.nap_time = 100;
                    thegops.start_single_step = false;
                    thegops.start_level_fielname = NULL;
                    enum GameStates states;
                    states = WORM_GAME_ONGOING;
                    //show List of all Levels
                    char** items = malloc(sizeof(char*)*(conf->level_list_size+2));
                    long items_size =conf->level_list_size;
                    items[items_size+1]= NULL;
                    if(items != NULL){
                        int cLevelIndex;
                        for(cLevelIndex = 0; cLevelIndex < conf->level_list_size;cLevelIndex++){
                            char name[256];
                            bzero(name, 256);
                            sprintf(name, "%s (%d)",conf->level_list[cLevelIndex].name,conf->level_list[cLevelIndex].difficultiy);
                            items[cLevelIndex] = malloc(sizeof(char)*(strlen(name)+1));
                            strcpy(items[cLevelIndex], name);
                        }
                        items[conf->level_list_size] = "add new Level File";
                        int selctedLevel = openSubMenu((LINES/2)+2, COLS/2, items,FALSE,NULL);
                        if(selctedLevel < items_size){
                            //open File
                            struct game_options thegops;
                            thegops.nap_time = 100;
                            thegops.start_single_step = false;
                            thegops.start_level_fielname = NULL;
                            enum GameStates game_state = WORM_GAME_ONGOING;
                            char* filename = malloc(sizeof(char)*(strlen(conf->user_level_base_dir)+strlen(conf->level_list[selctedLevel].name)+1));
                            sprintf(filename, "%s%s",conf->user_level_base_dir,conf->level_list[selctedLevel].name);
                            nodelay(stdscr, TRUE);
                            doLevel(&thegops, &states, filename, GAME_LOCAL_MULTIPLAYER, NULL);
                            free(filename);
                            nodelay(stdscr, FALSE);
                        }else{
                            //add another File
                            char file[50];
                            bzero(file, 50);
                            open_Dialog((LINES/2)+2, COLS/2, file, "file name:");
                            //config_add_Level(conf,file);
                        }
                        for(cLevelIndex = 0; cLevelIndex < items_size;cLevelIndex++){
                            free(items[cLevelIndex]);
                        }
                        free(items);
                    }
                }
                int j;
                for(j=0;j<7;j++){
                    menuClearRow((LINES/2)+1+j);
                }
            }while(i!=3);
            i = 1;
        }
        for(j=0;j<7;j++){
            menuClearRow((LINES/2)+1+j);
        }
    }while(i!=2);
    //displayStringInCenter(COLS/2, (LINES/2)+2, "--> SINGLEPLAYER", COLP_FOOD_1);
    //display Multiplayer
    //displayStringInCenter(COLS/2, (LINES/2)+4, "MULTIPLAYER", COLP_FOOD_1);
    //refresh();
    //napms(5000);
}

int openSubMenu(int startY,int centerX, char* stringItems[],bool remove,struct config* conf){
    int i = 0;
    int selectedI = 0;
    char buf[50];
    nodelay(stdscr, FALSE);
    int count;
    while(stringItems[i] != 0){
        i++;
    }
    count = i;
    i = 0;
    int ch;
    do{
        switch (ch) {
            case KEY_UP:
                selectedI--;
                if(selectedI < 0){
                    selectedI = 0;
                }
                break;
            case KEY_DOWN:
                selectedI++;
                if(selectedI > count-1){
                    selectedI = count-1;
                }
                break;
            case 265:
            case KEY_DL:
            case 127:
            case KEY_BACKSPACE:
            case 'X':
            case 'x':
                if(remove){
                    config_remove_Server(conf,selectedI);
                    return -1;
                }
                break;
            default:
                break;
        }
        i = 0;
        while(stringItems[i] != 0){
            if(i == selectedI){
                sprintf(buf,"--> %s <--",stringItems[i]);
                displayStringInCenter(startY+(i*2),centerX, buf, COLP_FOOD_2);
            }else{
                sprintf(buf,"    %s    ",stringItems[i]);
                displayStringInCenter(startY+(i*2),centerX, buf, COLP_FOOD_1);
            }
            i++;
        }
        refresh();
        ch = getch();
    }while(ch != '\r');
    return selectedI;
}

void open_Dialog(int startY,int centerX, char* buf, char* title){
    int ch = '\0';
    int ci = strlen(buf);
    do{
            //ch is Number      OR      ch is BIGLETTER   OR    normal Letter
        if((ch > 45 && 58 > ch) || (ch > 64 && 91 > ch) || (ch > 96 && 123 > ch)){
            int i;
            for(i = strlen(buf)+1;i>ci;i--){
                buf[i] = buf[i-1];
            }
            buf[ci] = ch;
            ci++;
        }else{
            switch (ch) {
                case 265:
                case KEY_DL:
                case 127:
                case KEY_BACKSPACE:
                    if(ci > 0){
                    ci--;
                        int i = ci;
                        while(buf[i+1] != '\0'){
                            buf[i]=buf[i+1];
                            i++;
                        }
                        buf[i] = '\0';
                    }
                    break;
                case KEY_LEFT:
                    if(ci > 0){
                    ci--;
                    }
                    break;
                case KEY_RIGHT:
                    if(ci < strlen(buf)){
                        ci++;
                    }
                    break;
                default:
                    break;
            }
        }
        menuClearRow(startY);
        displayStringInCenter(startY,centerX, title, COLP_FOOD_1);
        int len = strlen(buf);
        int startx = centerX -(len/2);
        int endx = startx + len;
        int i = 0;
        int x;
        menuClearRow(startY+2);
        for(x = startx; x < endx; x++){
            if(i == ci){
                menuplaceItem(startY+2, x, buf[i], COLP_CURSER_ON);
            }else{
                menuplaceItem(startY+2, x, buf[i], COLP_CURSER_OFF);
            }
            i++;
        }
        if(ci >= i){
            menuplaceItem(startY+2, x, ' ', COLP_CURSER_ON);
        }else{
            menuplaceItem(startY+2, x, ' ', COLP_CURSER_OFF);
        }
        refresh();
        ch = getch();
    }while (ch != '\r');
}
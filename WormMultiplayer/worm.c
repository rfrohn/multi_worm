// A simple variant of the game Snake
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//

#include <curses.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

#include "worm.h"
#include "options.h"
#include "prep.h"
#include "messages.h"
#include "worm_model.h"
#include "board_model.h"
#include "menu.h"
#include "config.h"

// ********************************************************************************************
// Constants, data structures
// ********************************************************************************************

// ********************************************************************************************
// Forward declarations of functions
// ********************************************************************************************
// This avoids problems with the sequence of function declarations inside the code.
// Note: this kind of problem is solved by header files later on!

// Management of the game
void initializeColors();
void readUserInput(struct worm* aworm,enum GameStates* agame_state );
//enum ResCodes doLevel(struct game_options* somegops,enum GameStates* agame_state, char* level_filename,bool network, struct networkInfo* network_info);

void changeColor();

// ********************************************************************************************
// Functions
// ********************************************************************************************

// ************************************
// Management of the game
// ************************************

// Initialize colors of the game
void initializeColors() {
    // Define colors of the game
    start_color();
    init_pair(COLP_USER_WORM,     COLOR_GREEN,    COLOR_BLACK);
    init_pair(COLP_FREE_CELL, COLOR_BLACK, COLOR_BLACK);
    init_pair(COLP_USER_WORM_C1,     COLOR_RED,    COLOR_BLACK);
    init_pair(COLP_USER_WORM_C2,     COLOR_BLUE,    COLOR_BLACK);
    init_pair(COLP_USER_WORM_C3,     COLOR_MAGENTA,    COLOR_BLACK);
    init_pair(COLP_FOOD_1, COLOR_YELLOW, COLOR_BLACK);
    init_pair(COLP_FOOD_2, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(COLP_FOOD_3, COLOR_CYAN, COLOR_BLACK);
    init_pair(COLP_BARRIER,COLOR_RED,COLOR_BLACK);
    init_pair(COLP_CURSER_ON, COLOR_MAGENTA, COLOR_GREEN);
    init_pair(COLP_CURSER_OFF, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(COLP_RUNE, COLOR_WHITE, COLOR_BLACK);
    
}

void readUserInput(struct worm* aworm, enum GameStates*  agame_state ) {
    int ch; // For storing the key codes
    
    if ((ch = getch()) > 0) {
        // Is there some user input?
        // Blocking or non-blocking depends of config of getch
        switch(ch) {
            case 'q' :    // User wants to end the show
                *agame_state = WORM_GAME_QUIT;
                break;
            case KEY_UP :// User wants up
                setWormHeading(aworm,WORM_UP);
                changeColor(aworm);
                break;
            case KEY_DOWN :// User wants down
                setWormHeading(aworm,WORM_DOWN);
                changeColor(aworm);
                break;
            case KEY_LEFT :// User wants left
                setWormHeading(aworm,WORM_LEFT);
                changeColor(aworm);
                break;
            case KEY_RIGHT :// User wants right
                setWormHeading(aworm,WORM_RIGHT);
                changeColor(aworm);
                break;
            case 's' : // User wants single step
                nodelay(stdscr, FALSE);  // We simply make getch blocking
                break;
            case ' ' : // Terminate single step; make getch non-blocking again
                nodelay(stdscr, TRUE);   // Make getch non-blocking again
                break;
            case 'g': // For development: let the worm grow by BONUS 3 elements
                growWorm(aworm, BONUS_3);
                break;
        }
    }
    return;
}

void readSecUserInput(struct worm* aworm,struct worm* aworm2, enum GameStates*  agame_state ) {
    int ch; // For storing the key codes
    
    if ((ch = getch()) > 0) {
        // Is there some user input?
        // Blocking or non-blocking depends of config of getch
        switch(ch) {
            case 'q' :    // User wants to end the show
                *agame_state = WORM_GAME_QUIT;
                break;
            case KEY_UP :// User wants up
                setWormHeading(aworm,WORM_UP);
                changeColor(aworm);
                break;
            case KEY_DOWN :// User wants down
                setWormHeading(aworm,WORM_DOWN);
                changeColor(aworm);
                break;
            case KEY_LEFT :// User wants left
                setWormHeading(aworm,WORM_LEFT);
                changeColor(aworm);
                break;
            case KEY_RIGHT :// User wants right
                setWormHeading(aworm,WORM_RIGHT);
                changeColor(aworm);
                break;
            case 't' : // User wants single step
                nodelay(stdscr, FALSE);  // We simply make getch blocking
                break;
            case ' ' : // Terminate single step; make getch non-blocking again
                nodelay(stdscr, TRUE);   // Make getch non-blocking again
                break;
            case 'g': // For development: let the worm grow by BONUS 3 elements
                growWorm(aworm, BONUS_3);
                break;
            case 'w' :// User wants up
                setWormHeading(aworm2,WORM_UP);
                changeColor(aworm2);
                break;
            case 's' :// User wants down
                setWormHeading(aworm2,WORM_DOWN);
                changeColor(aworm2);
                break;
            case 'a' :// User wants left
                setWormHeading(aworm2,WORM_LEFT);
                changeColor(aworm2);
                break;
            case 'd':// User wants right
                setWormHeading(aworm2,WORM_RIGHT);
                changeColor(aworm2);
                break;
            case 'e': // For development: let the worm grow by BONUS 3 elements
                growWorm(aworm2, BONUS_3);
                break;
        }
    }
    return;
}

enum ResCodes doLevel(struct game_options* somegops,enum GameStates* agame_state, char* level_filename,enum GameMode network, struct networkInfo* network_info) {
    struct worm userworm,otherworm;
    struct board theboard;
    
    enum ResCodes res_code; // Result code from functions
    enum ResCodeNetwork res_code_net;
    bool end_level_loop;    // Indicates whether we should leave the main loop
    
    struct pos bottomLeft;   // Start positions of the worm
    if(network == GAME_REMOTE_MULTIPLAYER){
    displayStringInCenter((LINES/2)+1, COLS/2, "building Level...(sync BOUNDS)", COLP_USER_WORM);
    refresh();
        napms(100);
    }
    struct netBoard netBoard;
    netBoard.board = &theboard;
    theboard.offset_col = 0;
    theboard.offset_row = 0;
    // Setup the board
    res_code = initializeBoard(&theboard);
    if( res_code != RES_OK) {
        return res_code;
    }
    res_code_net = RES_NET_OK;
    if(network == GAME_LOCAL || network == GAME_LOCAL_MULTIPLAYER){
        // Setup the Level
        res_code = initializeLevelFromFile(&theboard, level_filename);
        // "/Users/rune/Documents/xcode/rworm/worm00/Worm090/pirates-doubledoom.level.4"
        if( res_code != RES_OK) {
            return res_code;
        }
    }else if(network == GAME_REMOTE_MULTIPLAYER){
        // Set up right bounds
        if(network_info->networkside == SIDE_SERVER){
            char buff[256];
            net_listen_message(network_info, &res_code_net, buff);
            int clientRows,clientCols;
            net_get_board_bounds(buff,&clientRows,&clientCols);
            if(theboard.last_col > clientCols){
                netBoard.cols = clientCols;
                netBoard.board->offset_col = ((theboard.last_col-clientCols)/2)+1;
            }else{
                netBoard.cols = theboard.last_col;
                netBoard.board->offset_col = 1;
            }
            if(theboard.last_row > clientRows){
                netBoard.rows = clientRows;
                netBoard.board->offset_row= ((theboard.last_row-clientRows)/2)+1;
            }else{
                netBoard.rows = theboard.last_row;
                netBoard.board->offset_row = 1;
            }
            net_create_final_board_bounds_massage(buff,&netBoard);
            net_send_message(network_info, &res_code_net, buff);
        }else if(network_info->networkside == SIDE_CLIENT){
            char buff[256];
            net_create_board_bounds_massage(buff,&theboard);
            net_send_message(network_info, &res_code_net, buff);
            net_listen_message(network_info, &res_code_net, buff);
            net_get_final_board_bounds(buff,&netBoard);
            int clientRows,clientCols;
            clientRows = netBoard.rows;
            clientCols = netBoard.cols;
            if(theboard.last_col > clientCols){
                netBoard.cols = clientCols;
                netBoard.board->offset_col = ((theboard.last_col-clientCols)/2)+1;
            }else{
                netBoard.cols = theboard.last_col;
                netBoard.board->offset_col = 1;
            }
            if(theboard.last_row > clientRows){
                netBoard.rows = clientRows;
                netBoard.board->offset_row= ((theboard.last_row-clientRows)/2)+1;
            }else{
                netBoard.rows = theboard.last_row;
                netBoard.board->offset_row = 1;
            }
        }
        res_code = initializeLevelForMuliplayer(&netBoard);
        if( res_code != RES_OK) {
            return res_code;
        }
        if(network){
            displayStringInCenter((LINES/2)+1, COLS/2, "create Food...", COLP_USER_WORM);
            refresh();
        }
        //set up Food
        if(network_info->networkside == SIDE_SERVER){
            char buff[256];
            createFood(buff, MIN_FOOD_ELEMENTS, netBoard.rows, netBoard.cols);
            napms(10);
            net_send_message(network_info, &res_code_net, buff);
            char* s = buff;
            s= s+2;
            addFood(&theboard, s);
        }else if(network_info->networkside == SIDE_CLIENT){
            char buff[256];
            net_listen_message(network_info, &res_code_net, buff);
            switch(buff[0]){
                    case NET_CODE_ADD_FOOD:
                    addFood(&theboard, &buff[2]);
                    break;
                    default:
                    return RES_NETERROR;
            }
        }
    }
    if(network == GAME_REMOTE_MULTIPLAYER){
        displayStringInCenter((LINES/2)+1, COLS/2, "wake THE WORM...", COLP_USER_WORM);
        refresh();
    }
    // There is always an initialized user worm.
    // Initialize the userworm with its size, position, heading.
    if(network == GAME_LOCAL){
        bottomLeft.y =  getLastRowOnBoard(&theboard);
        bottomLeft.x =  0;
        res_code = initializeWorm(&userworm,(theboard.last_row+1)*(theboard.last_col+1),WORM_INITIAL_LENGTH, bottomLeft , WORM_RIGHT, COLP_USER_WORM);
        if ( res_code != RES_OK) {
            return res_code;
        }
    }else if(network == GAME_LOCAL_MULTIPLAYER){
        bottomLeft.y =  getLastRowOnBoard(&theboard);
        bottomLeft.x =  0;
        res_code = initializeWorm(&userworm,(theboard.last_row+1)*(theboard.last_col+1),WORM_INITIAL_LENGTH, bottomLeft , WORM_RIGHT, COLP_USER_WORM);
        if ( res_code != RES_OK) {
            return res_code;
        }
        struct pos topLeft;
        topLeft.y =  0;
        topLeft.x =  0;
        res_code = initializeWorm(&otherworm,(theboard.last_row+1)*(theboard.last_col+1),WORM_INITIAL_LENGTH, topLeft , WORM_RIGHT, COLP_USER_WORM_C1);
        if ( res_code != RES_OK) {
            return res_code;
        }
    }else if(network == GAME_REMOTE_MULTIPLAYER){
        if(network_info->networkside == SIDE_SERVER){
            struct pos mypos,otherpos;
            mypos.y =  netBoard.rows-2;
            mypos.x =  0;
            otherpos.y = 0;
            otherpos.x = 0;
            char buff[256];
            bzero(buff, 256);
            net_create_create_worm_message(buff,mypos,WORM_RIGHT,otherpos,WORM_RIGHT);
            napms(10);
            otherpos.x += theboard.offset_col;
            mypos.x += theboard.offset_col;
            otherpos.y += theboard.offset_row;
            mypos.y += theboard.offset_row;
            net_send_message(network_info, &res_code_net, buff);
            //init server Worm
            res_code = initializeWorm(&userworm,(theboard.last_row+1)*(theboard.last_col+1),WORM_INITIAL_LENGTH, mypos , WORM_RIGHT, COLP_USER_WORM);
            if ( res_code != RES_OK) {
                return res_code;
            }
            res_code = initializeWorm(&otherworm,(theboard.last_row+1)*(theboard.last_col+1),WORM_INITIAL_LENGTH, otherpos , WORM_RIGHT, COLP_USER_WORM_C1);
            if ( res_code != RES_OK) {
                return res_code;
            }
            
        }else if(network_info->networkside == SIDE_CLIENT){
            char buff[256];
            net_listen_message(network_info, &res_code_net, buff);
            if(res_code_net != RES_NET_OK){
                return RES_NETERROR;
            }
            if(buff[0] == NET_CODE_START_POS_INFORMATION){
                //dissmiss s:
                char* s = &buff[2];
                char * pch;
                char pat[2];
                struct netWormInfo cWorm;
                int i = 0;
                int j = 0;
                pat[0] = NET_CODE_SPLIT;
                pat[1] = '\0';
                pch = strtok (s,pat);
                while (pch != NULL)
                {
                    switch(i%3){
                        case 0:
                            // y information
                            cWorm.pos.y = atoi(pch)+theboard.offset_row;
                            break;
                        case 1:
                            // x Information;
                            cWorm.pos.x = atoi(pch)+theboard.offset_col;
                            break;
                        case 2:
                            cWorm.worm_dir = atoi(pch);
                            if(j == 0){
                                res_code = initializeWorm(&otherworm, (theboard.last_row+1)*(theboard.last_col+1), WORM_INITIAL_LENGTH, cWorm.pos, cWorm.worm_dir, COLP_USER_WORM);
                                j++;
                            }else{
                                res_code = initializeWorm(&userworm, (theboard.last_row+1)*(theboard.last_col+1), WORM_INITIAL_LENGTH, cWorm.pos, cWorm.worm_dir, COLP_USER_WORM_C1);
                            }
                    }
                    i++;
                    pch = strtok (NULL, pat);
                }
            }
        }
    }
    napms(500);
    displayStringInCenter((LINES/2)+1, COLS/2, "          3         ", COLP_USER_WORM);
    refresh();
    napms(500);
    displayStringInCenter((LINES/2)+1, COLS/2, "          2         ", COLP_USER_WORM);
    refresh();
    napms(500);
    displayStringInCenter((LINES/2)+1, COLS/2, "          1         ", COLP_USER_WORM);
    refresh();
    napms(500);
    displayStringInCenter((LINES/2)+1, COLS/2, "       Los!       ", COLP_USER_WORM);
    refresh();
    napms(500);
    displayStringInCenter((LINES/2)+1, COLS/2, "              ", COLP_USER_WORM);
    // Show worm at its initial position
    showWorm(&theboard,&userworm);
    if(network == GAME_REMOTE_MULTIPLAYER){
        showWorm(&theboard, &otherworm);
        nodelay(stdscr, TRUE);
    }else if(network == GAME_LOCAL_MULTIPLAYER){
        showWorm(&theboard, &otherworm);
    }
    
    // Display all what we have set up until now
    refresh();
    //To DO:countdown till Start!
    int breakCauseUser = -1;
    // Start the loop for this level
    end_level_loop = false; // Flag for controlling the main loop
    int i = 0;
    while(!end_level_loop) {
        i++;
        // Process optional user input
        if(network == GAME_LOCAL_MULTIPLAYER){
            readSecUserInput(&userworm,&otherworm,agame_state);
        }else{
            readUserInput(&userworm,agame_state);
        }
        
        if(network == GAME_REMOTE_MULTIPLAYER){
            if(network_info->networkside == SIDE_SERVER){
                char buff[256];
                net_listen_message(network_info, &res_code_net, buff);
                net_updateNetWorm(&otherworm,buff);
                net_createNetWormCode(&userworm,buff);
                net_send_message(network_info, &res_code_net, buff);
                if(res_code_net != RES_NET_OK){
                    res_code = RES_NETERROR;
                    break;
                }
                //create new Food
                char buff2[256];
                bzero(buff2, 256);
                int count = 0;
                if(i%50 == 0){
                    count = 1;
                }
                createFood(buff2, count, netBoard.rows, netBoard.cols);
                napms(10);
                net_send_message(network_info, &res_code_net, buff2);
                char* s = buff2;
                s= s+2;
                addFood(&theboard, s);
            }else if(network_info->networkside == SIDE_CLIENT){
                char buff[256];
                net_createNetWormCode(&userworm,buff);
                net_send_message(network_info, &res_code_net, buff);
                net_listen_message(network_info, &res_code_net, buff);
                net_updateNetWorm(&otherworm,buff);
                if(res_code_net != RES_NET_OK){
                    res_code = RES_NETERROR;
                    break;
                }
                //get new Food
                char buff2[256];
                bzero(buff2, 256);
                net_listen_message(network_info, &res_code_net, buff2);
                switch(buff2[0]){
                    case NET_CODE_ADD_FOOD:
                        addFood(&theboard, &buff2[2]);
                        break;
                    default:
                        return RES_NETERROR;
                }
            }
        }
        
        if ( *agame_state == WORM_GAME_QUIT ) {
            end_level_loop = true;
            continue; // Go to beginning of the loop's block and check loop condition
        }
        
        // Process userworm
        // Clean the tail of the worm
        cleanWormTail(&theboard,&userworm);
        if(network == GAME_REMOTE_MULTIPLAYER || network == GAME_LOCAL_MULTIPLAYER){
            cleanWormTail(&theboard,&otherworm);
        }
        // Now move the worm for one step
        moveWorm(&theboard,&userworm,agame_state);
        // Bail out of the loop if something bad happened
        if ( *agame_state != WORM_GAME_ONGOING ) {
            breakCauseUser = 0;
            end_level_loop = true;
            continue; // Go to beginning of the loop's block and check loop condition
        }
        if(network == GAME_REMOTE_MULTIPLAYER || network == GAME_LOCAL_MULTIPLAYER){
            moveWorm(&theboard,&otherworm,agame_state);
            // Bail out of the loop if something bad happened
            if ( *agame_state != WORM_GAME_ONGOING ) {
                if(breakCauseUser == 0){
                    breakCauseUser = 1;
                }
                end_level_loop = true;
                continue; // Go to beginning of the loop's block and check loop condition
            }
        }
        // Show the worm at its new position
        showWorm(&theboard,&userworm);
        if(network == GAME_REMOTE_MULTIPLAYER || network == GAME_LOCAL_MULTIPLAYER){
            showWorm(&theboard, &otherworm);
        }
        // END process userworm
        // Inform user about position and length of userworm in status window
        showStatus(&theboard,&userworm);
        // Sleep a bit before we show the updated window
        napms(somegops->nap_time);
        
        // Display all the updates
        refresh();
        // Are we done with that level?
        if (getNumberOfFoodItems(&theboard) == 0) {
            end_level_loop = true;
        }
        // Start next iteration
    }
    
    // Preset res_code for rest of the function
    res_code = RES_OK;
    
    // For some reason we left the control loop of the current level.
    // Check why according to game_state
    switch (*agame_state) {
        case WORM_GAME_ONGOING:
            if (getNumberOfFoodItems(&theboard) == 0) {
                showDialog("Sie habe diese Runde erfolgreich beendet!!!",
                           "Bitte Taste druecken");
            }else if(res_code_net == RES_NET_ERROR){
                showDialog("Netzwerk fehler :'(","Bitte Taste druecken");
            }else{
                showDialog("Interner Fehler!", "Bitte Taste druecken");
                // Correct result code
                res_code = RES_INTERNAL_ERROR;
            }
            break;
        case WORM_GAME_QUIT:
            //User must have typed 'q' for qui t
            showDialog("Sie haben die aktuelle Runde abgebrochen!",
                       "Bitte Taste druecken");
            break;
        case WORM_CRASH:
            if(breakCauseUser == 0){
                showDialog("Sie haben das Spiel verloren, "
                           " weil Sie in die Barriere gefahren sind",
                           "Bitte Taste druecken");
            }else{
                showDialog("Sie haben das Spiel gewonnen, "
                           " weil dein Gegner in die Barriere gefahren ist",
                           "Bitte Taste druecken");
            }
            break;
        case WORM_OUT_OF_BOUNDS:
            if(breakCauseUser == 0){
                showDialog("Sie haben das Spiel verloren."
                           "weil Sie das Spielfeld verlassen haben",
                           "Bitte Taste druecken");
            }else{
                showDialog("Sie haben das Spiel gewonnen."
                           "weil dein Gegner das Spielfeld verlassen hat",
                           "Bitte Taste druecken");
            }
            break;
        case WORM_CROSSING:
            if(breakCauseUser == 0){
                showDialog("Sie haben das Spiel verloren,"
                           "weil Sie einen Wurm gekreuzt haben",
                           "Bitte Taste druecken");
            }else{
                showDialog("Sie haben das Spiel gewonnen,"
                           "weil dein Gegner einen Wurm gekreuzt hat",
                           "Bitte Taste druecken");
            }
            break;
        default:
            showDialog ("Interner Fehler!", "Bitte Taste druecken");
            // Set error result code. This should never happen.
            res_code = RES_INTERNAL_ERROR;
    }
    // Normal exit point
    
    //Remove the worm from display and board
    removeWorm(&theboard, &userworm);
    //Because initializeWorm allocates memory
    cleanupWorm(&userworm);
    
    cleanupBoard(&theboard);
    return res_code;
}

//change the worm Color into a new Color
void changeColor(struct worm* aworm){
    //int oldColor = aworm->wcolor;
    //while(oldColor == aworm->wcolor){
    //    aworm->wcolor = (rand() % COLP_USER_WORM_SPAN )+1;
    //}
}

enum ResCodes playGame(int arge, char* argv[]){
    enum GameStates game_state;
    enum ResCodes res_code; // Result code from functions
    struct game_options thegops; // For options passed on the command line
    // An array of filenames for Level descriptors
    // The list must be terminated by a NULL pointer!
    char * level_list[] = {
        "basic.level.1",
        "squaredance.level.2",
        "pirates-doom.level.3",
        NULL
    };
    char * baseDir = "/Users/rune/Documents/xcode/rworm/worm00/Worm090/";
    int cur_level;
    
    // At the beginnung of the level, we still have a chance to win
    game_state = WORM_GAME_ONGOING;
    
    // Read the command line options
    res_code =readCommandLineOptions(&thegops, arge, argv);
    if ( res_code != RES_OK) {
        return res_code; // Error: leave early
    }
    if (thegops.start_single_step){
        nodelay(stdscr, FALSE); // make getch tobe a blocking ca11
    }
    // Play the game
    if(thegops.start_level_fielname != NULL){
        //User provided a filname on the command line.
        //Play only this Level
        res_code = doLevel(&thegops,&game_state,thegops.start_level_fielname,GAME_LOCAL,NULL);
        //From here in we no longer need thegops.start_level_filaname
        //Free the memory allocated by strdup in option.c
        free(thegops.start_level_fielname);
    }else{
        //Play all the levels in the level_list
        cur_level = 0;
        while(level_list[cur_level] != NULL && res_code == RES_OK && game_state == WORM_GAME_ONGOING){
            //char buf[50];
            //sprintf(buf, "%s%s",baseDir,level_list[cur_level]);
            res_code = doLevel(&thegops,&game_state,"/Users/rune/Documents/xcode/rworm/worm00/Worm090/pirates-doom.level.3",GAME_LOCAL,NULL);
            cur_level++;
        }
        if(res_code != RES_OK){
            switch(res_code){
                    case RES_FAILED:
                    showDialog("schade :'( hat wohl nicht geklappt versuch es noch ein mal!", "Bitte Taste druecken");
                    break;
                case RES_INTERNAL_ERROR:
                    showDialog("Interner Fehler :/ Tut mir leid das hätte nicht passieren dürfen", "Bitte Taste druecken");
            }
        }else if(level_list[cur_level] == NULL && game_state == WORM_GAME_ONGOING){
            showDialog("Herzlichen Glückwunch! Du hast alle Level gemeistert", "Bitte Taste druecken");
        }else if(game_state == WORM_GAME_QUIT){
            showDialog("Tschö mit Ö. Komm bald wieder, Ja?", "Bitte Taste druecken");
        }else{
            showDialog("Das war knapp. Das nächste mal schaffst du auch das letzte Level!", "Bitte Taste druecken");
        }
    }
    return res_code;
}

// END WORM_DETAIL
// ********************************************************************************************

// ********************************************************************************************
// MAIN
// ********************************************************************************************

int main(int argc, char* argv[]) {
    enum ResCodes res_code;         // Result code from functions
    
    // Here we start
    initializeCursesApplication();  // Init various settings of our application
    initializeColors();             // Init colors used in the game
    
    // Maximal LINES and COLS are set by curses for the current window size.
    // Note: we do not cope with resizing in this simple examples!
    
    // Check if the window is large enough to display messages in the message area
    // a has space for at least one line for the worm
    
    
    if ( LINES < ROWS_RESERVED + MIN_NUMBER_OF_ROWS || COLS < MIN_NUMBER_OF_COLS ) {
        // Since we not even have the space for displaying messages
        // we print a conventional error message via printf after
        // the call of cleanupCursesApp()
        cleanupCursesApp();
        printf("Das Fenster ist zu klein: wir brauchen mindestens %dx%d\n",
               MIN_NUMBER_OF_COLS, MIN_NUMBER_OF_ROWS +ROWS_RESERVED);
        res_code = RES_FAILED;
    } else {
        //napms(1000);
        struct config conf;
        getConfig(&conf, "wormm.conf");
        start_Menu(&conf);
        cleanUpConfig(&conf);
        //res_code = playGame(argc, argv);
        cleanupCursesApp();
    }
    
    return res_code;
}

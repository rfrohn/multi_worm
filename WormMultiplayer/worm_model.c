// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The worm 

#include <curses.h>
#include <stdlib.h>
#include "messages.h"
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"

// Initialize the worm
enum ResCodes initializeWorm(struct worm* aworm,int len_max,int len_cur,struct pos headpos,enum WormHeading dir, enum ColorPairs color) {
  aworm->dx = 1;
  aworm->dy = 0;
  // Local variables for loops etc .
  int i;

  // Initialize last usable index to len max -1: aworm->maxindex
  aworm->maxindex = len_max-1; 
  // Current last usable index in array. May grow upto maxindex
  aworm->cur_lastindex = len_cur - 1;

  // Initialize headindex: aworm->headindex
  aworm->headindex = 0;

    // Initialize the array for element positions
    // Allocate an array of the worms length
    if( (aworm->wormpos = malloc(sizeof(struct pos)*len_max)) == NULL){
        showDialog("Abbruch: Zu wenig SPeicher","Bitte eine Taste druecken");
        exit(RES_FAILED); //No memory -> direct exit
    }
    
  // Markallelements as unused in the arrays of positions
  // aworm->wormpos[] and aworm->wormpos[]
  // An unused position in the array is marked with code UNUSED POS ELEM
  for(i = 0; i < aworm->maxindex; i++){
    aworm->wormpos[i].x = UNUSED_POS_ELEM;
    aworm->wormpos[i].y = UNUSED_POS_ELEM;
  }


  // Initialize position of worms head
  aworm->wormpos[aworm->headindex] = headpos;

  // Initialize the heading of the worm
  setWormHeading(aworm,dir); 

  // Initialze color of the worm
  aworm->wcolor = color;

  return RES_OK;
}

void growWorm(struct worm* aworm, enum Boni growth) {
  // Play it save and inhibit surpassing the bound
  if (aworm->cur_lastindex + growth <= aworm->maxindex){
    aworm->cur_lastindex += growth;
  }else {
    aworm->cur_lastindex = aworm->maxindex;
  }
}
// Show the worms's elements on the display
// Simple version
void showWorm(struct board* aboard,struct worm* aworm) {
    // Due to our encoding we just need to show the head element
    // All other elements are already displayed
    //change new Head_element to Headelement
    //change last Head_element(first Inner Element) to Inner Element
    //change last Element to Tail
    int tailElementIndex = (aworm->headindex+1)%(aworm->cur_lastindex+1);
    if(aworm->wormpos[tailElementIndex].x == UNUSED_POS_ELEM){
        tailElementIndex = 0;
    }
    int firstInnerElementIndex = (aworm->headindex-1+(aworm->cur_lastindex+1))%(aworm->cur_lastindex+1);
    placeItem(
              aboard,
              aworm->wormpos[aworm->headindex].y,
              aworm->wormpos[aworm->headindex].x,
              BC_USED_BY_WORM,
              SYMBOL_WORM_HEAD_ELEMENT,
              aworm->wcolor);
    if(aworm->wormpos[firstInnerElementIndex].y != -1 && aworm->wormpos[firstInnerElementIndex].x != -1){
        placeItem(
                  aboard,
                  aworm->wormpos[firstInnerElementIndex].y,
                  aworm->wormpos[firstInnerElementIndex].x,
                  BC_USED_BY_WORM,
                  SYMBOL_WORM_INNER_ELEMENT,
                  aworm->wcolor);
    }
    if(aworm->wormpos[tailElementIndex].y != -1 && aworm->wormpos[tailElementIndex].x != -1){
        placeItem(
                  aboard,
                  aworm->wormpos[tailElementIndex].y,
                  aworm->wormpos[tailElementIndex].x,
                  BC_USED_BY_WORM,
                  SYMBOL_WORM_TAIL_ELEMENT,
                  aworm->wcolor);
    }
}


int getWormLength(struct worm* aworm){
  return aworm->cur_lastindex+1;
}

void cleanWormTail(struct board* aboard,struct worm* aworm){
  int tailindex;
  // Compute tailindex
  tailindex = (aworm->headindex+1)%(aworm->cur_lastindex+1);
  // Check the array of warm elements.
  // Is the array element at tailindex already in use?
  // Checking either array aworm->wormpos or aworm->wormpos is enough.
  if ( aworm->wormpos[tailindex].x != UNUSED_POS_ELEM ) {
    // YES: place a SYMBOL_FREE_CELL at the tail's position
    placeItem(aboard,aworm->wormpos[tailindex].y , aworm->wormpos[tailindex].x ,
        BC_FREE_CELL,SYMBOL_FREE_CELL,aworm->wcolor);
  }
}
void moveWorm(struct board* aboard,struct worm* aworm,enum GameStates* agame_state) {
  // Compute and store new head position according to current heading.

  //int headpos_y = aworm->wormpos[aworm->headindex].y + aworm->dy;
  struct pos headpos = {aworm->wormpos[aworm->headindex].y + aworm->dy,aworm->wormpos[aworm->headindex].x +aworm->dx};

  // Check if we would leave the display if we move the worm's head according
  // to worm's last direction.
  // We are not allowed to leave the display's window.
  if (headpos.x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.x > getLastColOnBoard(aboard) ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y < 0) {  
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y > getLastRowOnBoard(aboard) ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
      // We will stay within bounds.
      // Check if the worms head hits any items at the new position on the board .
      // Hitting food is good. hitting barriers or warm elements is bad.
      switch ( getContentAt(aboard,headpos) ) {
          case BC_FOOD_1:
              *agame_state = WORM_GAME_ONGOING;
              // Grow warm according to food item digested
              growWorm(aworm , BONUS_1);
              decrementNumberOfFoodItems(aboard);
              break;
          case BC_FOOD_2:
              // Grow warm according to food item digested
              growWorm(aworm , BONUS_2);
              decrementNumberOfFoodItems(aboard);
              break;
          case BC_FOOD_3:
              // Grow warm according to food item digested
              growWorm(aworm , BONUS_2);
              decrementNumberOfFoodItems(aboard);
              break;
          case BC_BARRIER:
              // That' s bad: stop game
              *agame_state = WORM_CRASH;
              break;
          case BC_USED_BY_WORM:
              // That's bad: stop game
              *agame_state = WORM_CROSSING;
              break;
          default:
              // Without default case we get a warning message.
          {;} // Do nothing. C syntax dictates some statement. here.
      }
  }
    
      /*if (isInUseByWorm(aworm,headpos))
      // That' s bad: stop game
      *agame_state = WORM_CROSSING;*/
  //Check the status of *agame_state
  // Go on if nothing bad happened
  if ( *agame_state == WORM_GAME_ONGOING ) {
    // So all is well: we did not hit anything bad and did not leave the
    // window. --> Update the warm structure.
    // Increment aworm->headindex
    // Go round if end of warm is reached (ring buffer)
      aworm->headindex++;
      if(aworm->headindex > aworm->cur_lastindex){
          aworm->headindex = 0;
      }
    // Store new coordinates af head element in worm structure
    aworm->wormpos[aworm->headindex] = headpos;
  }
}
// A simple collision detection
/*bool isInUseByWorm(struct worm* aworm,struct pos new_headpos) {
  int i;
  bool collision = false;
  i = aworm->headindex;
  do{

    // Campare the position of the current warm element with the new_headpos
    if(aworm->wormpos[i].x==new_headpos.x && aworm->wormpos[i].y==new_headpos.y ){
      collision = true;
    }
    i = (i-1+(aworm->maxindex+1))%(aworm->maxindex+1);
  } while ( i != aworm->headindex &&
      aworm->wormpos[i].x != UNUSED_POS_ELEM);
  // Return what we found out.
  return collision;
}*/

void removeWorm(struct board* aboard, struct worm* aworm){
    int i;
    i = aworm->headindex;
    do{
        placeItem(aboard, aworm->wormpos[i].y, aworm->wormpos[i].x, BC_FREE_CELL, SYMBOL_FREE_CELL, COLP_FREE_CELL);
        i = (i-1+(aworm->maxindex+1))%(aworm->maxindex+1);
    } while ( i != aworm->headindex &&
             aworm->wormpos[i].x != UNUSED_POS_ELEM);

}

void cleanupWorm(struct worm* aworm){
    free(aworm->wormpos);
}

// Setters
void setWormHeading(struct worm* aworm,enum WormHeading dir) {
  switch(dir) {
    case WORM_UP :// User wants up
      aworm->dx=0;
      aworm->dy=-1;
      break;
    case WORM_DOWN :// User wants down
      aworm->dx=0;
      aworm->dy=1;
      break;
    case WORM_LEFT      :// User wants left
      aworm->dx=-1;
      aworm->dy=0;
      break;
    case WORM_RIGHT      :// User wants right
      aworm->dx=1;
      aworm->dy=0;
      break;
  }
} 
// Getters
struct pos getWormHeadPos(struct worm* aworm) {
  // Structures are passed by value!
  // -> we return a copy here
  return aworm->wormpos[aworm->headindex];
}

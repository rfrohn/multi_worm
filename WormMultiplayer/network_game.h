//
//  network_game.h
//  WormXCODE
//
//  Created by Rune Frohn on 04.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#ifndef network_game_h
#define network_game_h

#include <stdio.h>
#include "server.h"
#include "client.h"
#include "worm_model.h"
#include "worm.h"
#include "options.h"

#define NET_CODE_SPLIT ':'
#define NET_CODE_ADD_FOOD 'f'
#define NET_CODE_START_POS_INFORMATION 's'
#define NET_CODE_WORMPOS 'w'
#define NET_CODE_BOARD_BOUNDS 'b'
#define NET_CODE_FINAL_BOARD_BOUNDS 'B'

enum networkSide{
    SIDE_SERVER,
    SIDE_CLIENT,
};

enum ResCodeNetwork{
    RES_NET_OK,
    RES_NET_ERROR,
};

struct networkInfo{
    enum networkSide networkside;
    void* networkAdapter;
};
struct netWormInfo{
    struct pos pos;
    int worm_dir;
};
struct netWormPlayer{
    char* name;
    struct worm playerWorm;
    void* nextElement;
    
};
extern void start_network_game(struct networkInfo network_info);
extern void net_send_message(struct networkInfo* network_info, enum ResCodeNetwork* res_code, char buffer[256]);
extern void net_listen_message(struct networkInfo* network_info, enum ResCodeNetwork* res_code, char buffer[256]);
extern void net_create_create_worm_message(char* buff,struct pos mypos, enum WormHeading mydir,struct pos otherpos,enum WormHeading otherdir);

extern void net_createNetWormCode(struct worm* userworm,char buff[256]);
extern void net_updateNetWorm(struct worm* aworm,char buff[256]);

void net_create_board_bounds_massage(char* buff,struct board* theboard);
void net_get_board_bounds(char* buff,int* clientRows,int* clientCols);

void net_create_final_board_bounds_massage(char* buff,struct netBoard* theboard);
void net_get_final_board_bounds(char* buff,struct netBoard* theboard);
#endif /* network_game_h */


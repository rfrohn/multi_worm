//
//  client.c
//  WormXCODE
//
//  Created by Rune Frohn on 04.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#include "client.h"

void open_client(enum ClientRes* res_code, struct Client* aClient,char* hostname, int port){
    struct sockaddr_in serv_addr;
    struct hostent *server;
    aClient->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (aClient->sockfd < 0){
        *res_code = RES_ERROR_C_OPEN_SOCKET;
        return;
    }
    server = gethostbyname(hostname);
    if (server == NULL) {
        *res_code = RES_ERROR_NO_HOST;
        return;
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
    serv_addr.sin_port = htons(port);
    if (connect(aClient->sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0){
        *res_code = RES_ERROR_C_CONNECT;
        return;
    }
}
void c_listenForMessage(enum ClientRes* res_code,struct Client* aClient,char buffer[256]){
    bzero(buffer,256);
    int n = read(aClient->sockfd,buffer,255);
    if (n < 0){
        *res_code = RES_ERROR_READING;
    }

}
void writeMessage(enum ClientRes* res_code,struct Client*aClient,char* s){
    int n = write(aClient->sockfd,s,strlen(s));
    if (n < 0){
        *res_code = RES_ERROR_SEND;
    }
}
void closeConnection(struct Client* aClient){
    close(aClient->sockfd);
}
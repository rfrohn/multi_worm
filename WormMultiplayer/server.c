//
//  server.c
//  WormXCODE
//
//  Created by Rune Frohn on 04.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#include "server.h"


void open_server(enum ServerRes* res_code,struct Server* aServer,int port){
    struct sockaddr_in serv_addr;
    aServer->sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (aServer->sockfd < 0){
        *res_code = RES_ERROR_OPEN_SOCKET;
        return;
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(port);
    if (bind(aServer->sockfd, (struct sockaddr *) &serv_addr,
             sizeof(serv_addr)) < 0){
        *res_code = RES_ERROR_BIND;
        return;
    }
    return;
}
void listenForClients(enum ServerRes* res_code,struct Server* aServer){
    socklen_t clilen;
    struct sockaddr_in cli_addr;
    listen(aServer->sockfd,5);
    clilen = sizeof(cli_addr);
    aServer->newsocketfd = accept(aServer->sockfd,
                       (struct sockaddr *) &cli_addr,
                       &clilen);
    if (aServer->newsocketfd < 0){
        *res_code = RES_ERROR_CONNECT;
        return;
    }else{
        aServer->connected = true;
    }
}

void s_listenForMessage(enum ServerRes* res_code,struct Server* aServer,char buffer[256]){
    bzero(buffer,256);
    int n;
    n = read(aServer->newsocketfd,buffer,255);
    if (n < 0){
        *res_code = RES_ERROR_LISTEN;
    }
    //printf("Here is the message: %s\n",buffer);
}

void sendMessage(enum ServerRes* res_code,struct Server* aServer,char* s){
    if(aServer->connected){
        int n = write(aServer->newsocketfd,s,strlen(s));
        if (n < 0){
            *res_code = RES_ERROR_SENDING_MESSAGE;
        }
    }else{
        *res_code = RES_ERROR_NOT_CONNECTED;
    }
}

void closeServer(struct Server* aServer){
    if(aServer->connected){
        close(aServer->newsocketfd);
        aServer->connected = false;
    }
    close(aServer->sockfd);
}
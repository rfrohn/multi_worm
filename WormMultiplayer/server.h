//
//  server.h
//  WormXCODE
//
//  Created by Rune Frohn on 04.12.15.
//  Copyright © 2015 Rune Frohn. All rights reserved.
//

#ifndef server_h
#define server_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>

struct Server {
    int sockfd;
    int newsocketfd;
    bool connected;
};
enum ServerRes{
    RES_ERROR_OPEN_SOCKET,
    RES_ERROR_BIND,
    RES_ERROR_CONNECT,
    RES_ERROR_SENDING_MESSAGE,
    RES_ERROR_NOT_CONNECTED,
    RES_ERROR_LISTEN,
    RES_SOK,
    
};
extern void open_server(enum ServerRes* res_code,struct Server* aServer,int port);
extern void listenForClients(enum ServerRes* res_code,struct Server* aServer);
extern void s_listenForMessage(enum ServerRes* res_code,struct Server* aServer,char buffer[256]);
extern void sendMessage(enum ServerRes* res_code,struct Server* aServer,char* s);
extern void closeServer(struct Server* aServer);

#endif /* server_h */
